package GoTestTools

import (
	"fmt"
	"testing"

	"github.com/pkg/errors"
)

var testStrings []string = []string{"TEST", "ẞönDérZäíſĉh€Ń", "1234567890", "", "\n", "//\\/\\!\\//\n// \\ !! ! "}

const (
	SingleVerbose = iota
	Verbose
	Concise
	Quiet
)

var VerboseMode = Concise

func Assert(success bool) error {
	if !success {
		return errors.New("assertion failed!")
	} else {
		return nil
	}
}

func Test(success bool, errMsg string, t *testing.T) {
	t.Helper()
	err := Assert(success)
	if err != nil {
		WrapErr(err, errMsg, t)
	}
}

func TestAgainstStrings(set func(s string) error, get func() (string, error), errMsg string, t *testing.T) {
	t.Helper()
	for _, testString := range testStrings {
		err := set(testString)
		if err != nil {
			WrapErr(err, "failed to set string", t)
		} else {
			result, err := get()
			if err != nil {
				WrapErr(err, "failed to get string", t)
			} else {
				errorMsg := errMsg + ": failed at string \"" + testString + "\"; is " + result
				Test(result == testString, errorMsg, t)
			}
		}
	}
}

func WrapErr(err error, msg string, t *testing.T) {
	t.Helper()
	switch VerboseMode {
	case Quiet:
		t.Fail()
	case SingleVerbose:
		VerboseMode = Concise
		fallthrough
	case Verbose:
		t.Error(fmt.Sprintf("%+v", errors.Wrap(err, msg)))
	case Concise:
		t.Error(errors.Wrap(err, msg))
	}
}
